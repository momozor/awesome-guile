# Awesome Guile ![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)
A curated list of awesome GNU Guile resources. The ones marked with a star :star: are recommended to try and use.

If you want to add anything new to the list, then please open a pull request. :-)

## General Purpose Libraries
* [guile-lib](https://www.nongnu.org/guile-lib/)

## GNU Guile Official Website
* :star: [GNU Guile Website](https://www.gnu.org/software/guile/)

## Emacs extensions for Guile development
* :star: [geiser](https://www.nongnu.org/geiser/)

## GNU Guile recommended versions
* :star: Stable [Guile 2.2.4](https://ftp.gnu.org/gnu/guile/guile-2.2.4.tar.gz)
* Unstable/Latest/Nightly [Guile 2.9.1 with JIT-enabled for x86-64](http://alpha.gnu.org/gnu/guile/guile-2.9.1.tar.gz)

## Build Tools/Templates
* :star: [guile-skeleton](https://gitlab.com/OrangeShark/guile-skeleton)

## Extensions
* [aiscm](https://github.com/wedesoft/aiscm)

## File Formats
* [guile-json](https://github.com/aconchillo/guile-json)
* [guile-commonmark](https://github.com/OrangeShark/guile-commonmark)
* [guile-csv](https://github.com/NalaGinrut/guile-csv)

## Game Framework
* [chickadee](https://dthompson.us/projects/chickadee.html)

## Web
* [artanis](https://web-artanis.com/)
* [guile-oauth](https://github.com/opencog/guile-dbi)

## Command Line
* [guile-ncurses](https://www.gnu.org/software/guile-ncurses/)

## Database
* [guile-dbi](https://github.com/opencog/guile-dbi)

## Networking
* [guile-ssh](https://github.com/artyom-poptsov/guile-ssh)
* [guile-irc](https://github.com/fbs/guile-irc)
